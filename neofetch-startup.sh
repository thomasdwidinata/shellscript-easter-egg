# This script requires 'neofetch' installed on the system. On Linux, you can just install it from the package manager. On Mac, you can use Homebrew to install this package.
# How this script will work is to generate a possibility whether it should show a neofetch everytime the terminal is fired.
# This function is to generate random numbers from 0 to 1 by mod the random number by 2.
function rand {
	number=$RANDOM
	let "number %= 2"
	return $number
}
if rand
then
	neofetch
fi
